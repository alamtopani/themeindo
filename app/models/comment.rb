class Comment < ActiveRecord::Base
	include Tree
	belongs_to :user, foreign_key: 'user_id'
	belongs_to :commentable, polymorphic: true

	before_save :prepare_set

	default_scope {where(status: true)}
	
	scope :latest, -> {order("created_at DESC")}
	scope :oldest, -> {order("created_at ASC")}

	scope :verify, -> {where(status: true)}
	scope :checked, -> {where(checked: true)}
	scope :not_checked, -> {where(checked: false)}

	def prepare_set
		self.commentable_type = 'Catalog'
	end

	def status?
		return '<span class="label label-success">Already Checked</span>'.html_safe if self.checked == true
		return '<span class="label label-warning">Not Yet Checked</span>'.html_safe if self.checked == false
	end
end
