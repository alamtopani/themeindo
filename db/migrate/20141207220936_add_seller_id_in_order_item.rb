class AddSellerIdInOrderItem < ActiveRecord::Migration
  def change
  	add_column :order_items, :seller_id, :integer
  	add_index :order_items, :seller_id
  end
end
