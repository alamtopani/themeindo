class Member::FavoritesController < Member::MemberApplicationController
	defaults resource_class: Favorite, collection_name: 'favorites', instance_name: 'favorite'
	include Backend::ActionMultiple

	add_breadcrumb "Home", :root_path
	add_breadcrumb "Dashboard", :member_home_path
	add_breadcrumb "My Favorites", :member_favorites_path

	def index
		@favorites = collection.latest.where(user_id: current_user.id)
		@collection = @favorites.page(page).per(per_page)
	end

	def create
		filter = Favorite.where(catalog_id: params[:catalog_id], user_id: current_user.id)
    if filter.present?
      redirect_to catalog_path(params[:catalog_id]), alert: "Cannot save to favorites, Because you already save this item before"
    else
			build_resource
			resource.catalog_id = params[:catalog_id]
	    resource.user_id = current_user.id

	    resource.save
	    create! do |format|
	      if resource.errors.empty?
	        format.html { redirect_to member_favorites_path}
	      else 
	        flash[:errors] = resource.errors.full_messages
	        format.html { redirect_to :back}
	      end
	    end
	  end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end