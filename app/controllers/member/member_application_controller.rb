class Member::MemberApplicationController < ResourcesController
	layout 'member'
	before_filter :authenticate_user!
	protect_from_forgery with: :exception
	before_filter :prepare_data_user
	
	private
		def prepare_data_user
			@profile = current_user.try(:profile)
			@address = @profile.try(:address)
		end
end