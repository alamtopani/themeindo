class Backend::CatalogCategoriesController < Backend::BackendApplicationController
	defaults resource_class: CatalogCategory, collection_name: 'catalog_categories', instance_name: 'catalog_category'
	include Backend::ActionMultiple

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	protected
    def collection
      @collection ||= end_of_association_chain
   	end
end