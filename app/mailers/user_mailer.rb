class UserMailer < ActionMailer::Base
	default from: 'themeindo@gmail.com'
	default to: 'themeindo@gmail.com'

	# CATALOG USER REGISTER

	def welcome_join(user)
		@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Welcome join to themeindo')
	end

	def send_user_to_staff(user)
		@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Your Account has change to staff')
	end

	# USER SET FEATURED

	def send_user_to_featured(user)
		@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Your account already be featured')
	end

	# CATALOG TICKET

	def send_ticket(ticket)
		@logo = "#{root_url}assets/logo.png"
		@ticket = ticket
		mail(to: 'themeindo@gmail.com', from: ticket.email, subject: "Ticket - #{ticket.departement}")
	end

	# REPLY TICKET

	def send_reply_ticket(message)
		@logo = "#{root_url}assets/logo.png"
		@message = message
		mail(to: message.to, subject: "Replies Tickets - #{message.try(:ticket).try(:code)}")
	end

	# CATALOG MAILER

	def send_catalog(catalog)
		@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Request New Catalog - #{catalog.user.username}")
	end

	def send_request_catalog(catalog)
		@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(from: catalog.user.email, subject: "Request Verification - #{catalog.user.username}")
	end

	def send_catalog_verified(catalog)
		@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Verified New Catalog - #{catalog.title}")
	end

	def send_catalog_featured(catalog)
		@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Featured New Catalog - #{catalog.title}")
	end

	def send_catalog_comment(comment)
		@logo = "#{root_url}assets/logo.png"
		@comment = comment
		mail(to: comment.commentable.user.email, subject: "New Comment for your item - #{comment.commentable.title}")
	end

	def send_invoice(order)
		@logo = "#{root_url}assets/logo.png"
		@order = order
		mail(to: @order.user.email, subject: "Invoice Order Success - #{order.code}")
	end

	def send_subscribe(sub, catalogs)
		@logo = "#{root_url}assets/logo.png"
		@sub = sub
		@catalogs = catalogs
		mail(to: @sub.email, subject: "Promotion Items This Week" )
	end

end