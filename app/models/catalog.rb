class Catalog < ActiveRecord::Base
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  is_impressionable
  acts_as_votable
  ratyrate_rateable "price"

	include CatalogScope
  before_create :prepare_code
  after_initialize :populate_galleries

  has_attached_file :file_catalog
  # validates_attachment_presence :file_catalog
  # validates_attachment_content_type :file_catalog, :content_type => [ 'application/pdf','text/plain', "application/zip", "application/x-zip", "application/x-zip-compressed" ]

  has_attached_file :thumbnail, styles: {
                      small:    '128x128>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :thumbnail, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

	def arrange_as_array(options={}, hash=nil)
    hash ||= arrange(options) unless hash.is_a? Array
    arr = []

    hash.each do |node, children|
      arr << node
      arr += arrange_as_array(options, children) unless children.nil?
    end
    arr
  end

  include CatalogStatus
  include CacheIt

  def score
    self.get_upvotes.size
  end

  def build_image(media_title, position=nil)
    gallery = self.galleries.build({title: media_title})
    gallery.position = position
  end

  private
    include CatalogFilter

    def prepare_code
      self.code = SecureRandom.urlsafe_base64
    end

    def populate_galleries
      if self.galleries.count < 3
        [
          'Front Cover',
          'Responsive Cover',
          'Homepage'
        ].each_with_index do |media_title, index|
          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end
    end

end

