class OrdersController < ApplicationController
	add_breadcrumb "Home", :root_path

  def paid
    if order = Order.pay(params[:token], current_user, current_order, params[:PayerID])
    	if current_order
    		UserMailer.send_invoice(current_order).deliver
				session[:order_id] = nil
			end
      redirect_to review_path(params[:token]), notice: 'Thanks, your order is successfully paid, please check your email to see evidence of the invoice has been paid, or you can see the details below!'
    else
      redirect_to orders_path, alert: 'Something went wrong. Please try again.'
    end
  end

  def revoked
  	redirect_to orders_path, alert: 'Your order has been cancelled.'
  end

  def ipn
    if payment = Payment.find_by_transaction_id(params[:txn_id])
      payment.receive_ipn(params)
    else
      Payment.create_by_ipn(params)
    end
  end

  def review
    add_breadcrumb "Success complete order"

    @order = Order.find_by(payment_token: params[:id])
    unless @order && current_user
      redirect_to root_path, notice: 'You do not have permission to access this page'
    end
  end
	
	def show
		add_breadcrumb "My Order Items", orders_path

		@order = current_order
		@order_items = @order.order_items

    if @order
	    @paypal = PaypalInterface.new(@order)
		  @paypal.express_checkout
		  if @paypal.express_checkout_response.success?
		    @paypal_url = @paypal.api.express_checkout_url(@paypal.express_checkout_response)
		  else
		    redirect_to root_path, notice: "Something went wrong. You must order item first before view this page, Please try again."
		  end
		end
  end
end