module TicketSearch
	extend ActiveSupport::Concern

	included do

		scope :filter_by_departement, ->(departement) do
			return if departement.blank?
			where(departement: departement)
		end

		scope :filter_by_priority, ->(priority) do
			return if priority.blank?
			where(priority: priority)
		end

		scope :filter_by_key, ->(key) do
			return if key.blank?
			query_opts = [
				"LOWER(tickets.code) LIKE LOWER(:key)",
				"LOWER(users.username) LIKE LOWER(:key)"
			].join(' OR ')
			bonds.where(query_opts, {key: "%#{key}%"})
		end

		scope :filter_search, ->(params) do 
			return all if params.blank?
			filter_by_key(params[:key])
			.filter_by_departement(params[:departement])
			.filter_by_priority(params[:priority])
		end
	end

end