module UserConfig
	extend ActiveSupport::Concern

	included do
		devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, #:confirmable,
         		:validatable, :lockable, :timeoutable, :omniauthable, omniauth_providers: [:facebook], :authentication_keys => [:login]

    after_initialize :after_initialized
    ratyrate_rater
    acts_as_voter

    has_one :profile, foreign_key: 'user_id', dependent: :destroy
    has_many :comments, foreign_key: 'user_id', dependent: :destroy
	  has_many :catalogs, foreign_key: 'user_id', dependent: :destroy
	  has_many :tickets, foreign_key: 'user_id', dependent: :destroy
	  has_many :favorites, foreign_key: 'user_id', dependent: :destroy
	  has_many :messages, foreign_key: 'user_id', dependent: :destroy
	  has_many :orders, foreign_key: 'user_id', dependent: :destroy
	  has_many :order_items, foreign_key: 'seller_id', dependent: :destroy
	  has_many :testimonials, foreign_key: 'user_id', dependent: :destroy
	  has_one :subscribe, foreign_key: 'email', dependent: :destroy

	  accepts_nested_attributes_for :profile, reject_if: :all_blank
	  accepts_nested_attributes_for :comments, reject_if: :all_blank
	  accepts_nested_attributes_for :catalogs, reject_if: :all_blank
	  accepts_nested_attributes_for :tickets, reject_if: :all_blank
	  accepts_nested_attributes_for :favorites, reject_if: :all_blank
	  accepts_nested_attributes_for :messages, reject_if: :all_blank
	  accepts_nested_attributes_for :orders, reject_if: :all_blank
	  accepts_nested_attributes_for :order_items, reject_if: :all_blank
	  accepts_nested_attributes_for :subscribe, reject_if: :all_blank
	  accepts_nested_attributes_for :testimonials, reject_if: :all_blank

	  validates_uniqueness_of :username, :email
	  
	  after_validation :move_friendly_id_error_to_name
	  before_save :downcase_username

	  scope :member, ->{ where(type: 'Member') }
	  scope :admin, ->{ where(type: 'Admin') }
	  scope :featured, ->{ where(featured: true) }

	  scope :latest, -> {order("users.created_at DESC")}
		scope :oldest, -> {order("users.created_at ASC")}

		scope :bonds, -> { eager_load({profile: :address}) }

	end

	def admin?
		self.type == "Admin"
	end

	def member?
		self.type == "Member"
	end

	def staff?
		self.type == "Member" && self.staff == true
	end

	def verified_staff
    self.staff ? 'Staff' : 'Member'
  end

	def change_verified_status!
    if self.staff
      self.staff = false
      self.save
    elsif !self.staff
      self.staff = true
      self.save
    end
  end

  def featured?
	  self.featured = true
  end

	def change_featured_status!
    if self.featured
      self.featured = false
      self.save
    elsif !self.featured
      self.featured = true
      self.save
    end
  end

  def featured_status
    self.featured ? 'Featured' : 'Un Featured'
  end

	protected
		def after_initialized
			self.profile = Profile.new if self.profile.blank?
		end

		def downcase_username
			self.username = self.username.downcase if username_changed?
		end

		def move_friendly_id_error_to_name
      errors.add friendly_id_config.base, *errors.delete(:friendly_id) if errors[:friendly_id].present?
    end
end