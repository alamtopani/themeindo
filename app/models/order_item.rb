class OrderItem < ActiveRecord::Base
  belongs_to :catalog, foreign_key: 'catalog_id'
  belongs_to :order, foreign_key: 'order_id'
  belongs_to :user

  scope :latest, ->{order(created_at: :desc)}
  scope :verified, ->{where(order_item_status: true)}

  # validate :catalog_present
  # validate :order_present

  before_save :finalize
  
  def price
    if persisted?
      self[:price]
    else
      catalog.price
    end
  end

  def total_price
    price * quantity
  end

  def saller
    catalog.user.id
  end

  private 
  	def catalog_present
	    if catalog.nil?
	      errors.add(:catalog, "is not valid or is not active.")
	    end
	  end

	  def order_present
	    if order.nil?
	      errors.add(:order, "is not a valid order.")
	    end
	  end

	  def finalize
	    self[:price] = price
	    self[:total_price] = quantity * self[:price]
      self.user_id = User.current.id
      self.seller_id = saller
	  end
end
