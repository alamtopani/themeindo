class Ticket < ActiveRecord::Base
	before_create :prepare_code
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  belongs_to :user, foreign_key: 'user_id'

  has_many :messages, foreign_key: 'ticket_id', dependent: :destroy
  accepts_nested_attributes_for :messages, reject_if: :all_blank

  scope :latest, -> {order(created_at: :desc)}
	scope :oldest, -> {order(created_at: :asc)}
	scope :checked, -> {where(status: true)}
	scope :not_checked, -> {where(status: false)}

	scope :bonds, -> {
    eager_load({
      user: [{profile: :address}]
    })
  }

	def priority?
		return '<span class="label label-important">High</span>'.html_safe if self.priority == 'High'
		return '<span class="label label-warning">Medium</span>'.html_safe if self.priority == 'Medium'
		return '<span class="label label-info">Low</span>'.html_safe if self.priority == 'Low'
	end

	def status?
		return '<span class="label label-success">Already Checked</span>'.html_safe if self.status == true
		return '<span class="label label-warning">Not Yet Checked</span>'.html_safe if self.status == false
	end

	private
    include TicketSearch

    def prepare_code
      self.code = SecureRandom.hex(6) if self.code.blank?
    end
end
