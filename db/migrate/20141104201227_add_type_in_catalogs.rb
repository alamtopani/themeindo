class AddTypeInCatalogs < ActiveRecord::Migration
  def change
  	add_column :catalogs, :catalog_type, :string
  	add_index :catalogs, :catalog_type
  end
end
