module CatalogFilter
	extend ActiveSupport::Concern
	included do
		scope :filter_by_title, ->(title) do
      return if title.blank?
      query_opts = [
        "LOWER(catalogs.title) LIKE LOWER(:key)",
        "LOWER(users.username) LIKE LOWER(:key)"
      ].join(' OR ')
      bonds.where(query_opts, {key: "%#{title}%"})
    end

    scope :filter_by_catalog_category_type, ->(catalog_category_type) do
      return if catalog_category_type.blank?
      bonds.where("catalog_categories.name =?", catalog_category_type)
    end

    scope :filter_by_catalog_category, ->(catalog_category) do
      return if catalog_category.blank?
      category_bonds.where("catalog_categories.name =?", catalog_category)
    end

    scope :filter_by_catalog_type, ->(catalog_type) do
      return if catalog_type.blank?
      where(catalog_type: catalog_type)
    end

    scope :filter_by_price, ->(price_from, price_to) do
      return if price_from.blank? && price_to.blank?
      return where('price <= ? OR price >= ?', price_to, price_from) if price_from.blank? || price_to.blank?
      where('price <= ? AND price >= ?', price_to, price_from) if price_from.present? && price_to.present?
    end

    scope :filter_by_sort, ->(sort_by) do
      return if sort_by.blank?
      self.send(sort_by)
    end

    scope :filter_by_published, ->(published) do
      return if published.blank?
      self.send(published)
    end

    scope :filter_by_featured, ->(featured) do
      return if featured.blank?
      where(featured: featured)
    end

    scope :filter_by_status, ->(status) do
      return if status.blank?
      where(status: status)
    end

    scope :filter_search, ->(params)  do
      return all if params.blank?
      filter_by_title(params[:title])
      .filter_by_catalog_type(params[:catalog_type])
      .filter_by_catalog_category_type(params[:catalog_category_type])
      .filter_by_catalog_category(params[:catalog_category])
      .filter_by_price(params[:price_from].blank? ? nil : params[:price_from].to_i, params[:price_to].blank? ? nil : params[:price_to].to_i)
      .filter_by_sort(params[:sort_by])
      .filter_by_published(params[:published])
      .filter_by_featured(params[:featured])
      .filter_by_status(params[:status])
    end
	end
end