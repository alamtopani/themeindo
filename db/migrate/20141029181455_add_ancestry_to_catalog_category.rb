class AddAncestryToCatalogCategory < ActiveRecord::Migration
  def change
    add_column :catalog_categories, :ancestry, :string
    add_index :catalog_categories, :ancestry
  end
end
