Themeindo::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :users, 
    controllers: { 
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions',
      confirmations: "confirmations"
    }, 
    path_names: { 
      sign_in:  'login', 
      sign_out: 'logout', 
      sign_up:  'register' 
    }
    
  concern :startup_backend, Startup::BackendRoutes.new
  namespace :backend do
    concerns :startup_backend
  end

  concern :startup_member, Startup::MemberRoutes.new
  namespace :member do
    concerns :startup_member
  end

  root 'publics#home'
  get '/backend',                   to: 'backend#home',                     as: 'backend_home'
  get '/member',                    to: 'member#home',                      as: 'member_home'
  get '/profile',                   to: 'member#profile',                   as: 'profile'
  get 'search',                     to: 'publics#search',                   as: 'search'
  get 'portfolio/:id',              to: 'publics#portfolio',                as: 'portfolio'
  get "like/:id",                   to: "publics#upvote",                   as: 'like'
  get "document_download/:id",      to: "publics#document_download",        as: 'document_download'
  get "landing_page/:id",           to: "publics#landing_page",             as: 'landing_page'
  
  get "catalog_category_child",     to: "catalogs#catalog_category_child",  as: 'catalog_category_child'

  # Success review order
  get "review/:id",                 to: "orders#review",                    as: 'review'

  resources :catalogs do
    member do
      get 'comments'
      get 'support'
      get 'ticket'
      put "like",     to: "catalogs#upvote"
      match :upvote, path: 'like', via: [:get, :post, :put]
    end
  end

  resource :orders do
    collection do
      get :paid
      get :revoked
      post :ipn
    end
  end

  resources :order_items, only: [:create, :update, :destroy]
  resources :comments
  resources :tickets
  resources :subscribes

  # namespace :api do
  #   namespace :v1 do 
  #     resources :catalogs, only: [:index, :show] do 
  #       member do
  #         get 'comments'
  #         get 'support'
  #         get 'ticket'
  #         put "like",     to: "catalogs#upvote"
  #         match :upvote, path: 'like', via: [:get, :post, :put]
  #       end
  #     end
  #   end
  # end


end
