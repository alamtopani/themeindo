class Message < ActiveRecord::Base
	extend FriendlyId
  friendly_id :subject, use: [:slugged, :finders]

  belongs_to :user
  belongs_to :ticket

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :new_message, ->{where(status: false)}

  def status?
		return '<span class="label label-info">New</span>'.html_safe if self.status == false
		return '<span class="label label-default">Check</span>'.html_safe if self.status == true
	end
end
