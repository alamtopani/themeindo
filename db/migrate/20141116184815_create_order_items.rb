class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.references :catalog, index: true
      t.references :order, index: true
      t.decimal :price
      t.integer :quantity
      t.decimal :total_price

      t.timestamps
    end
  end
end
