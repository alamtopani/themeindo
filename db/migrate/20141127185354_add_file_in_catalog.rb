class AddFileInCatalog < ActiveRecord::Migration
  def self.up
    add_attachment :catalogs, :file_catalog
  end

  def self.down
    remove_attachment :catalogs, :file_catalog
  end
end
