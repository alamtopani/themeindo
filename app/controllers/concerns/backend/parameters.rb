module Backend::Parameters
	extend ActiveSupport::Concern

	protected
		def permitted_params
			instance_params = params[resource_instance_name]
			params.permit(resource_instance_name => Permitable.controller(params[:controller]))
		end
end