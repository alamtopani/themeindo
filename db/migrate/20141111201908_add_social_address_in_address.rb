class AddSocialAddressInAddress < ActiveRecord::Migration
  def change
  	add_column :addresses, :facebook_url, :string
  	add_column :addresses, :twitter_url, :string
  end
end
