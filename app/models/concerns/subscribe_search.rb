module SubscribeSearch
	extend ActiveSupport::Concern

	included do

		scope :filter_by_key, ->(key) do
			return if key.blank?
			query_opts = "LOWER(subscribes.email) LIKE LOWER(:key)"
			where(query_opts, {key: "%#{key}%"})
		end

		scope :filter_search, ->(params) do 
			return all if params.blank?
			filter_by_key(params[:key])
		end
	end

end