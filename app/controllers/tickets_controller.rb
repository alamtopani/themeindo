class TicketsController < ResourcesController
	defaults resource_class: Ticket, collection_name: 'tickets', instance_name: 'ticket'

	def create
	  build_resource
	  resource.user_id = current_user.id
	  resource.save
	  create! do |format|
	  	if resource.errors.empty?
        format.html { redirect_to :back}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
	  end
	end

	private
    def collection
      @tickets ||= end_of_association_chain.filter_search(params)
    end
end