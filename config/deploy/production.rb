# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
role :app, %w{themeindo@107.155.116.200}
role :web, %w{themeindo@107.155.116.200}
role :db,  %w{themeindo@107.155.116.200}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '107.155.116.200', user: 'themeindo', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.1.2'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/themeindo/www"
set :rails_env,       "production"
set :branch,          "production"

set :unicorn_config_path, "/home/themeindo/www/current/config/unicorn.rb"