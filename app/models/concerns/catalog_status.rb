module CatalogStatus
	extend ActiveSupport::Concern

	included do
		def featured?
	    self.featured = true
	  end

		def change_featured_status!
	    if self.featured
	      self.featured = false
	      self.save
	    elsif !self.featured
	      self.featured = true
	      self.save
	    end
	  end

	  def featured_status
	    self.featured ? 'Featured' : 'Un Featured'
	  end

		def status?
	    return '<button type="button" class="btn btn-labeled btn-success">
                <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Already Verified
              </button>'.html_safe if self.status == true
	    return '<button type="button" class="btn btn-labeled btn-warning">
                <span class="btn-label"><i class="glyphicon glyphicon-info-sign"></i></span>Not Yet Verified
              </button>'.html_safe if self.status == false
	  end

	  def change_verified_status!
	    if self.status
	      self.status = false
	      self.save
	    elsif !self.status
	      self.status = true
	      self.save
	    end
	  end

	  def published?
	    self.status == true
	  end

	  def unpublish!
	    self.status = false
	  end

	  def verified_status
	    self.status ? 'Verified' : 'Un Verified'
	  end

	end
end
