module CacheIt
	extend ActiveSupport::Concern
	module ClassMethods
		def cache_it(unique_key, time, &block)
      Rails.cache.fetch("#{unique_key}", expires_in: time) do
        block.call if block_given?
      end
    end
	end
end
