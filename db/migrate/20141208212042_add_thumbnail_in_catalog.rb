class AddThumbnailInCatalog < ActiveRecord::Migration
  def self.up
    add_attachment :catalogs, :thumbnail
  end

  def self.down
    remove_attachment :catalogs, :thumbnail
  end
end
