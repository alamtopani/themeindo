class Member::CatalogsController < Member::MemberApplicationController
	load_and_authorize_resource

	defaults resource_class: Catalog, collection_name: 'catalogs', instance_name: 'catalog'
	include Backend::ActionMultiple

	add_breadcrumb "Home", :root_path
	add_breadcrumb "Dashboard", :member_home_path
	add_breadcrumb "My Items", :member_catalogs_path

	def index
		@catalogs = collection.last_updated.where(user_id: current_user.id)
		@collection = @catalogs.page(page).per(per_page)
	end

	def new
		add_breadcrumb "New", new_member_catalog_path
	end

	def edit
		add_breadcrumb "Edit", edit_member_catalog_path(resource)
	end

	def create
		build_resource
		resource.catalog_category_id = params[:catalog_category_id]
    resource.user_id = current_user.id
    resource.save
    create! do |format|
      if resource.errors.empty?
      	UserMailer.send_catalog(resource).deliver
      	UserMailer.send_request_catalog(resource).deliver
        format.html { redirect_to member_catalogs_path}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
    end
	end

	def update
		resource.catalog_category_id = params[:catalog_category_id]
		resource.save
    update! do |format|
      if resource.errors.empty?
        format.html { redirect_to member_catalogs_path}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
    end
	end

	def document_download
	  @catalog = Catalog.find_by(code: params[:code])
    url = @catalog.file_catalog.url
    file_path = url.split("?").first
    if !file_path.nil?
			send_file "#{Rails.root}/public#{file_path}", :x_sendfile => true 
		else 
      redirect_to :back
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end