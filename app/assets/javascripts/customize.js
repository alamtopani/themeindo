$(window).load(function(){
  $('.loading').fadeOut(300);

  $('.home-testimonial img').load(function(){
    $('.image-responsive').each(function(){
      if( $(this).height == 0 ){
        var imgWidth = $(this).width() / 1.57;
        $(this).css('height', imgWidth);
      }else{
        var imgWidth = $(this).width() / 1.57;
        $(this).css('height', imgWidth);
      }
    });
  });

  $('*').click(function(){
    $('.avatar-responsive').each(function(){
      var avaWidth = $(this).outerWidth() / 1;
      $(this).css('height', avaWidth);
    });
  });

});


$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();

  $('.awesome-tooltip').tooltip({
      placement: 'left'
  });   

  $('#root_category').change(function(){
    var url = $(this).data('url');
    $.ajax({ url: url, method: 'GET', data: { category: $(this).val() }}).
      complete(function(data){
        $('#child_category').html(data.responseText);
      });
  });

  $('[data-toggle="popover"]').each(function(){
    $(this).popover({
      trigger: 'hover',
      placement: 'top',
      html : true, 
      content: function() {
        return $(this).children('.tooltips-content').html();
      }
    });
  });

  $('.image-responsive').each(function(){
    if( $(this).height == 0 ){
      var imgWidth = $(this).width() / 1.7;
      $(this).css('height', imgWidth);
    }else{
      var imgWidth = $(this).width() / 1.7;
      $(this).css('height', imgWidth);
    }
  });

  
  $('.pie-chart').easyPieChart({
    onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    },
    size: '90',
    trackColor: '#ffffff',
    barColor: '#0066C8',
    scaleColor: '',
    lineWidth: 10,
    lineCap:'circle',
    animate: 2000
  });

  $('.chart-earning').easyPieChart({
    onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    },
    size: '90',
    trackColor: '#ffffff',
    barColor: '#FF8200',
    scaleColor: '',
    lineWidth: 10,
    lineCap:'circle',
    animate: 2000
  });

  $(window).resize(function(){
    $('.image-responsive').each(function(){
      if( $(this).height == 0 ){
        var imgWidth = $(this).width() / 1.9;
        $(this).css('height', imgWidth);
      }else{
        var imgWidth = $(this).width() / 1.9;
        $(this).css('height', imgWidth);
      }
    });

    $('.home-carousel .item').each(function(){
      var topSlider = $('.home-carousel').outerWidth() / 2.8;
      $(this).css('height', topSlider);
    });
  }).resize();


  $(window).resize(function(){
    $('.avatar-responsive').each(function(){
      var avaWidth = $(this).outerWidth() / 1;
      $(this).css('height', avaWidth);
    });

    $('.thumb-responsive').each(function(){
      var avaWidth = $(this).outerWidth() / 1;
      $(this).css('height', avaWidth);
    });
  }).resize();

  $('.thumb-responsive:first-child').css('opacity', '1');
  $('.thumb-responsive').click(function(){
    $('.thumb-responsive').css('opacity', '0.5');
    $(this).css('opacity', '1');
  });


  $('#list').click(function(event){
    event.preventDefault();
    $('.wrapper-list-show > .item').addClass('list-view-item');
  });

  $('#grid').click(function(event){
    event.preventDefault();
    $('.wrapper-list-show > .item').removeClass('list-view-item');
    $('.wrapper-list-show > .item').addClass('grid-view-item');
  });

  $('.search-front .dropdown-menu').find('a').click(function(e) {
    e.preventDefault();
    var param = $(this).attr("href").replace("#","");
    var concept = $(this).text();
    $('.search-front span#search_concept').text(concept);
    $('.input-group #search_param').val(param);
  });

  $('.home-carousel .item').each(function(){
    var slideImg = $(this).children('img').attr('src');
    $(this).css('background-image', 'url("'+slideImg+'")');
  });

  // if( !$('#comment_comment').val() ){
  //   $(this).animate({height: "86px"});
  // };

  $('.comment-parent .click-comment-reply-link').each(function(){
    $(this).click(function(e){
      var formReply = $(this).parents('.wrapper-item-comment').find('.new_comment');
      e.preventDefault();
      formReply.slideDown(300);
    });
  });

  $(window).load(function(){
    $('.dynamic_select').bind('change', function () {
      var url = $(this).find(':selected').data('url')
      if (url) {
          window.location = url;
      }
      return false;
    });
  });

});

(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  var openReviewBtn = $('#open-review-box');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.hide();
    
  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
  });
});