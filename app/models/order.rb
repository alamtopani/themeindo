class Order < ActiveRecord::Base
  before_create :prepare_code
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  belongs_to :user, foreign_key: 'user_id'
  has_many :order_items, foreign_key: 'order_id', dependent: :destroy

  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

  scope :latest, ->{order(created_at: :desc)}
  scope :verified, ->{where(order_status_id: 2)}

  scope :bonds, -> {
    eager_load({
      user: [{profile: :address}]
    })
  }

  before_create :set_order_status
  before_save :update_subtotal

  def total
    order_items.collect { |oi| oi.valid? ? (oi.quantity * oi.price) : 0 }.sum
  end

  def status?
    return '<span class="label label-warning">In Progress</span>'.html_safe if self.order_status_id == 1
    return '<span class="label label-success">Paid</span>'.html_safe if self.order_status_id == 2
    return '<span class="label label-important">Cancelled</span>'.html_safe if self.order_status_id == 3
  end

  def in_progress?
    self.order_status_id == 1
  end

  def paid?
    self.order_status_id == 2
  end

  def set_payment_token(token)
    self.payment_token = token
  end

  def self.pay(token, user, ordered, payerID)
    begin
      order = self.find_by_code(ordered.code)
      order.payment_token = token
      order.payerID = payerID
      order.purchased_at = Time.now
      order.order_status_id = 2
      if order
        order.order_items.map.each do |item|
          item.order_item_status = true
          item.save
        end
      end
      order.save
      @paypal = PaypalInterface.new(order)
      @paypal.do_express_checkout
      @paypal.get_express_checkout
      return order
    rescue
      false
    end
  end

  def set_transaction(transaction, payer_name, payer_email, payer_address)
    begin
      ordered = self
      ordered.transaction_id = transaction
      ordered.payer_name = payer_name
      ordered.payer_email = payer_email
      ordered.payer_address = payer_address
      ordered.save
      return ordered
    rescue
      false
    end
  end

  private
    include OrderSearch
    
    def set_order_status
      self.order_status_id = 1
      self.user_id = User.current.id
    end

    def update_subtotal
      self[:total] = total
    end

    def prepare_code
      self.code = SecureRandom.hex(6) if self.code.blank?
    end
end
