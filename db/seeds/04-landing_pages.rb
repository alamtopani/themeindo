module SeedLandingPage
	LANDING_PAGE = ['Help Center','Terms of Use','About Themeindo','Careers','Contact Us']

	def self.seed 
		LANDING_PAGE.each do |page|
			landing = LandingPage.find_or_initialize_by(title: page)
			landing.status = true
			landing.save
		end
	end
end