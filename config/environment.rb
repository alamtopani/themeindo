# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

if Rails.env.production?  
  HOST_WO_HTTP = 'themeindo.net'
  paypal_host = 'https://www.sandbox.paypal.com'
else  
  HOST_WO_HTTP = 'localhost:3000'
  paypal_host = 'https://www.paypal.com'
end  

HOST = "http://#{HOST_WO_HTTP}"
