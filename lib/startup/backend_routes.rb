module Startup
	class BackendRoutes
		def call mapper, options={}
			mapper.resources :members do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
				mapper.member do 
					mapper.put 'change_position'
					mapper.put 'change_featured'
				end
			end
			mapper.resources :admins do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :catalog_categories do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :catalogs do
				mapper.collection do
					mapper.post 'action_multiple'
				end
				mapper.member do 
					mapper.get 'document_download'
					mapper.put 'change_verified'
					mapper.put 'change_featured'
				end
			end
			mapper.resources :comments do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :tickets do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :orders do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
				mapper.member do
					mapper.put 'change_order_status'
				end
			end
			mapper.resources :messages do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :subscribes do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :landing_pages do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :web_settings do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
		end
	end
end