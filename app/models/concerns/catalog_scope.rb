module CatalogScope
	extend ActiveSupport::Concern

	included do 
		belongs_to :user, foreign_key: 'user_id'
	  belongs_to :catalog_category_root, class_name: 'CatalogCategory', foreign_key: 'catalog_category_type_id'
	  belongs_to :catalog_category_child, class_name: 'CatalogCategory', foreign_key: 'catalog_category_id' 

		has_many :comments, as: :commentable, dependent: :destroy
	  has_many :galleries, as: :galleriable, dependent: :destroy
	  has_one :favorite, foreign_key: 'catalog_id', dependent: :destroy
	  has_many :order_items #,foreign_key: 'catalog_id', dependent: :destroy

	  accepts_nested_attributes_for :favorite, reject_if: :all_blank, allow_destroy: true
	  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true
		accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true
	  #accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

	  scope :last_updated, -> {order("catalogs.updated_at DESC")}
		scope :latest, -> {order("catalogs.created_at DESC")}
		scope :oldest, -> {order("catalogs.created_at ASC")}

	  scope :low_to_high, -> {order("catalogs.price ASC")}
	  scope :high_to_low, -> {order("catalogs.price DESC")}
	    
	  scope :last_day, -> {where('catalogs.created_at >= :last_days_ago', :last_days_ago  => Time.now - 1.days)}
	  scope :last_week, -> {where('catalogs.created_at >= :last_weeks_ago', :last_weeks_ago  => Time.now - 7.days)}
	  scope :last_month, -> {where('catalogs.created_at >= :last_months_ago', :last_months_ago  => Time.now - 30.days)}
	  scope :last_year, -> {where('catalogs.created_at >= :last_years_ago', :last_years_ago  => Time.now - 360.days)}

	  scope :verify, -> {where("catalogs.status = ?", true)}
	  scope :featured, -> {where("catalogs.featured = ?", true)}

	  scope :bonds, -> {
	  	eager_load({ user: [{profile: :address}]}, :catalog_category_root)
	  }

	  scope :category_bonds, -> {
	  	eager_load(:catalog_category_child)
	  }
	  
	end
end