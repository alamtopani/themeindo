//= require backend/jquery.min
//= require backend/jquery-ui.min
//= require jquery_ujs
//= require backend/raphael-min
//= require backend/bootstrap
//= require backend/sparkline
//= require backend/morris.min
//= require backend/jquery.dataTables.min
//= require backend/jquery.masonry.min
//= require backend/jquery.imagesloaded.min
//= require backend/jquery.alertify.min
//= require backend/jquery.knob
//= require backend/fullcalendar.min
//= require backend/jquery.gritter.min
//= require backend/jquery.slimscroll.min
//= require cocoon
//= require jquery.raty
//= require ratyrate
//= require ckeditor/init
//= require ckeditor/config
//= require jquery-lazyload

$(document).ready(function(){
  $("img").show().lazyload({
     event: "lazyload",
     effect: "fadeIn",
     effectspeed: 500
  })
  .trigger("lazyload");
  
	$('#root_category').change(function(){
    var url = $(this).data('url');
    $.ajax({ url: url, method: 'GET', data: { category: $(this).val() }}).
	    complete(function(data){
	      $('#child_category').html(data.responseText);
	    });
  });

  $('form[data-remote]').bind('ajax:before', function(){
    for (instance in CKEDITOR.instances){
      CKEDITOR.instances[instance].updateElement();
    }
  });

});


