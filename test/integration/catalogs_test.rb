require 'test_helper'

class CatalogTest < ActionDispatch::IntegrationTest
	setup { host! 'themeindo.net:3000/api/v1' }

	test 'resturn list of all catalogs' do
		get '/catalogs'
		assert_equal 200, response.status
		refute_empty response.body
	end
end 