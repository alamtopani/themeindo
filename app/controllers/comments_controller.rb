class CommentsController < ResourcesController
	defaults resource_class: Comment, collection_name: 'comments', instance_name: 'comment'

	def create
		@comments_root = Comment.verify.latest
	  build_resource
	  resource.user_id = current_user.id
	  resource.save
	  create! do |format|
	  	if resource.errors.empty?
        format.html { 
        							UserMailer.send_catalog_comment(resource).deliver
        							redirect_to :back
        						}
        format.js
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
	  end
	end

	private
    def collection
      @comments ||= end_of_association_chain
    end
end