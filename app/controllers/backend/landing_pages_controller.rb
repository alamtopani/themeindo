class Backend::LandingPagesController < Backend::BackendApplicationController
	defaults resource_class: LandingPage, collection_name: 'landing_pages', instance_name: 'landing_page'
	include Backend::ActionMultiple

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end