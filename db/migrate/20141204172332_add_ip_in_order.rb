class AddIpInOrder < ActiveRecord::Migration
  def change
  	add_column :orders, :ip, :string
  	add_column :orders, :purchased_at, :datetime
  	add_column :orders, :transaction_id, :string
  end
end
