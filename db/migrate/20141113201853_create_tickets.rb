class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.string :name
      t.string :email
      t.string :subject
      t.string :departement
      t.string :problem
      t.string :priority
      t.text :message
      t.boolean :status

      t.timestamps
    end
  end
end
