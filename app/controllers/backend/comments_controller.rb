class Backend::CommentsController < Backend::BackendApplicationController
	defaults resource_class: Comment, collection_name: 'comments', instance_name: 'comment'
	include Backend::ActionMultiple
	
	def index
		@collection = collection.roots.latest.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end