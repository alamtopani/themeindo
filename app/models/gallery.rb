class Gallery < ActiveRecord::Base
	belongs_to :galleriable, polymorphic: true

  default_scope { order('galleries.position ASC') }
	scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}

	has_attached_file :file, styles: { 
                      large:    '700>', 
                      medium:   '500>', 
                      small:    '300>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

end
