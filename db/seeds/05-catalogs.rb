module SeedCatalog
  
  def self.seed
    url = 'http://themeforest.net/item/cleanco-cleaning-company-wordpress-theme/9460728'
    resp = RestClient.get URI.escape(url)
    @doc  = Nokogiri::HTML(resp.body)
    (1..30).to_a.each do |i|
      puts "Data -#{i}"
      content = @doc.css('.user-html')
      thumbnail = @doc.css('.avatar-wrapper')
      c = CatalogCategory.roots.first
      params  = {
                  user_id: User.last.id,
                  title: @doc.css('.page-title').text.strip.gsub(/\t/, '')+"-#{i}",
                  content: content.css('p').text.strip,
                  price: 80,
                  status: true,
                  featured: true,
                  thumbnail: URI.parse(thumbnail.css('img').attribute('src').value),
                  catalog_category_type_id: c.id,
                  catalog_category_id: c.children.first.id
                }

      catalog = Catalog.where(title: params[:title]).first
      if catalog.present?
        catalog.update_attributes(params)
        puts 'success updated'
      else
        catalog = Catalog.new(params)
        if catalog.save
          puts 'success saved' 
        else
          puts 'failed save'
        end
      end

      catalog = Catalog.where(title: params[:title]).first
      item_preview = @doc.css('.item-preview')

      (1..3).each do |item|
        galleries = {
          title: "Home Page #{item}",
          description: content.css('p').text.strip,
          file: URI.parse(item_preview.css('img').attribute('src').value),
          galleriable_id: catalog.id,
          galleriable_type: 'Catalog'
        }

        gallery = Gallery.new(galleries)
        if gallery.save
          puts "\timage saved"
        else
          puts "\timage fail to save"
        end
      end
    end
  end

end