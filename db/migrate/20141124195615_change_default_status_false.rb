class ChangeDefaultStatusFalse < ActiveRecord::Migration
  def change
  	change_column :tickets, :status, :boolean, default: false
  	change_column :catalogs, :status, :boolean, default: false
  end
end
