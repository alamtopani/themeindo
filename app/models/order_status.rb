class OrderStatus < EnumerateIt::Base
  associate_values(
    :pending	=> [1, 'In Progress'],
    :paid  		=> [2, 'Paid'],
    :declined => [3, 'Cancelled']
  )
end