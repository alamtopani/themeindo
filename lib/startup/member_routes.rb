module Startup
	class MemberRoutes
		def call mapper, options={}
			mapper.resources :members do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :catalogs do
				mapper.collection do
					mapper.post 'action_multiple'
				end
				mapper.member do 
					mapper.get 'document_download'
				end
			end
			mapper.resources :favorites do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :tickets do
				mapper.collection do
					mapper.delete 'destroy_multiple'
					mapper.get 'home'
				end
			end
			mapper.resources :orders do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :comments do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
			mapper.resources :messages do
				mapper.collection do
					mapper.delete 'destroy_multiple'
				end
			end
		end
	end
end