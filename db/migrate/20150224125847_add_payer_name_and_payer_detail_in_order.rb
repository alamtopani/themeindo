class AddPayerNameAndPayerDetailInOrder < ActiveRecord::Migration
  def change
  	add_column :orders, :payer_name, :string
  	add_column :orders, :payer_email, :string
  	add_column :orders, :payer_address, :string
  end
end
