class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :code
      t.references :user, index: true
      t.references :order_status, index: true
      t.text :token
      t.decimal :total

      t.timestamps
    end
  end
end
