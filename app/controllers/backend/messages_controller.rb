class Backend::MessagesController < Backend::BackendApplicationController
	defaults resource_class: Message, collection_name: 'messages', instance_name: 'message'
	include Backend::ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def create
		build_resource
		if params[:message][:ticket_id]
			@ticket = Ticket.find(params[:message][:ticket_id])
			@ticket.update(status: true)
			@ticket.save
		end
		resource.user_id = current_user.id
		resource.status = false
		resource.save
		create! do |format|
			if resource.errors.empty?
				UserMailer.send_reply_ticket(resource).deliver
        format.html { redirect_to :back }
      else 
        flash[:error] = 'Your Message Not Successfully Sent'
        format.html { redirect_to :back }
      end
		end
	end

	def destroy
		destroy! do |format|
			if resource.errors.empty?
        format.html { redirect_to :back }
      else 
        flash[:error] = 'Your Message Not Successfully Delete'
        format.html { redirect_to :back }
      end
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end