class MemberController < Member::MemberApplicationController
	layout 'member'
	add_breadcrumb "Home", :root_path

	def home
		add_breadcrumb "Dashboard", member_home_path
		add_breadcrumb "#{current_user.username.titleize}"

		@catalogs = current_user.catalogs.last_updated.limit(12)
	end

	def profile
		add_breadcrumb "Dashboard", member_home_path
		add_breadcrumb "Profile", profile_path
		add_breadcrumb "#{current_user.username.titleize}"
	end

end