class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :user, index: true
      t.string :to, index: true
      t.references :ticket, index: true
      t.string :subject
      t.text :message
      t.boolean :status, default: false
      t.string :slug

      t.timestamps
    end
  end
end
