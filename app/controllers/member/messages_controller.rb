class Member::MessagesController < Member::MemberApplicationController
	defaults resource_class: Message, collection_name: 'messages', instance_name: 'message'

	add_breadcrumb "Home", :root_path
	add_breadcrumb "Dashboard", :member_home_path
	add_breadcrumb "Messages", :member_messages_path

	def index
		@messages = collection.latest.where(to: current_user.email)
		@collection = @messages.page(page).per(per_page)
	end

	def show
		add_breadcrumb "Show"
		if resource
			resource.update(status: true)
			resource.save
		end
	end

	def create
		build_resource
		resource.user_id = current_user.id
		resource.status = true
		resource.save
		create! do |format|
			if resource.errors.empty?
        format.html { redirect_to :back }
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back }
      end
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end