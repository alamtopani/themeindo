module DocumentDownload
	extend ActiveSupport::Concern

	included do 
		def document_download
		  @catalog = Catalog.find_by(code: params[:id])
	    url = @catalog.file_catalog.url
	    file_path = url.split("?").first
	    if !file_path.nil?
				send_file "#{Rails.root}/public#{file_path}", :x_sendfile => true 
			else 
	      redirect_to :back
			end
		end
	end
end