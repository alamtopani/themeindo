class ChangeColumnToStringInCatalog < ActiveRecord::Migration
  def change
  	change_column :catalogs, :columns, :string
  end
end
