class AddOrderItemStatusInOrderItem < ActiveRecord::Migration
  def change
  	add_column :order_items, :order_item_status, :boolean
  end
end
