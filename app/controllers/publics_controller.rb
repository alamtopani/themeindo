class PublicsController < FrontendController
	add_breadcrumb "Home", :root_path
	impressionist

	include DocumentDownload

	def home
		@featureds = Catalog.featured.verify.last_updated.limit(9)
		@latest_catalogs = Catalog.verify.last_updated.limit(18)
		@featured_author = User.latest.featured.last
		@verify_featured_author = @featured_author.catalogs.verify.last_updated if @featured_author.present?
		@testimonials = Testimonial.verified.oldest
	end

	def search
		add_breadcrumb "Catalogs", catalogs_path
		add_breadcrumb "Results"

		@catalog_roots = Catalog.verify.last_updated.filter_search(params)
    @catalogs = @catalog_roots.page(page).per(per_page)
  end

  def portfolio
  	add_breadcrumb "Portfolio", portfolio_path(params)
		add_breadcrumb "#{params[:id]}"

  	@user = User.find_by(username: params[:id])
  	@profile = @user.profile
  	@address = @profile.address
  	@catalogs = @user.catalogs.verify.latest.page(page).per(per_page)
  	impressionist(@user, 'message', unique: [:session_hash])
  end

  def landing_page
  	add_breadcrumb "#{params[:id]}"
  end

  def upvote
	  @link = User.find(params[:id])
	  @success = @link.upvote_by current_user
	  if @success
	  	redirect_to :back, alert: "Your already like this portfolio!"
	  else
	  	redirect_to :back, error: "Your not success like this portfolio!"
	  end
	end

end
