class ChangeSubjectInTitleSupport < ActiveRecord::Migration
  def change
  	rename_column :tickets, :subject, :title
  end
end
