class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  protect_from_forgery with: :exception
  before_filter :prepare_settings
  before_filter :set_current_user
  before_filter :prepare_quick

  helper_method :backend_controller?
  helper_method :current_order

  layout :prepare_layout
  include DeviseFilter

  def backend_controller?
    is_a?(::BackendController)
  end

  def current_order
    if !session[:order_id].nil?
      Order.find(session[:order_id])
    else
      Order.new
    end
  end

  protected
    def per_page
      params[:per_page] ||= 18
    end

    def page
      params[:page] ||= 1
    end

  	def prepare_layout
      return 'devise'  if devise_controller?
      return 'backend' if backend_controller?
      return false if request.xhr?
      'application'
    end

    def prepare_settings
      WebSetting.cache_it('web_settings', 3.days) do
        setting = WebSetting.first
      end
      @setting = Rails.cache.read('web_settings')

      CatalogCategory.cache_it('catalog_categories_latest', 3.days) do
        catalog_categories = CatalogCategory.roots.oldest
      end

      @landing_pages = LandingPage.verified.oldest
    end

    def set_current_user
      User.current = current_user

      if current_user
        @sales = current_user.try(:order_items).verified.size
        @income = current_user.try(:order_items).verified.sum(:total_price)
      end
    end

    def prepare_quick
      @verify_latest_oder = Order.verified
      @unverify_latest_oder = Order.all

      @latest_catalogs = Catalog.latest.verify
      @verify_latest_catalogs = @latest_catalogs
      @unverify_latest_catalogs = Catalog.latest

      @root_tickets = Ticket.latest
      @latest_tickets = @root_tickets.not_checked
      @checked_tickets = @root_tickets.checked

      @root_comments = Comment.roots.latest
      @latest_comments = @root_comments.not_checked

      @latest_members = Member.latest

      # LATEST MESSAGE USER
      @latest_messages = Message.where(to: current_user.email).new_message.latest if current_user.present?
    end

end
