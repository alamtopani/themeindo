module CategoryAjax
	extend ActiveSupport::Concern

	def catalog_category_child
		id = CatalogCategory.find(params[:category])
		@lists = id.try(:children).alfa.map{|c| [c.name, c.id] }

		render inline: "<%= select_tag :catalog_category_id, options_for_select(@lists), { prompt: 'Select Category',class: 'form-control', id: 'child_category' } %>"
	end
end