class AddLayoutTypeInCatalog < ActiveRecord::Migration
  def change
  	add_column :catalogs, :layout_type, :string, index: true
  end
end
