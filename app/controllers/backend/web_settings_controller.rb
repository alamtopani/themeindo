class Backend::WebSettingsController < Backend::BackendApplicationController
	defaults resource_class: WebSetting, collection_name: 'web_settings', instance_name: 'web_setting'

	def index
		@collection = collection.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end