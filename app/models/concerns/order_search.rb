module OrderSearch
	extend ActiveSupport::Concern

	included do

		scope :filter_by_key, ->(key) do
			return if key.blank?
			query_opts = [
				"LOWER(orders.code) LIKE LOWER(:key)",
				"LOWER(users.username) LIKE LOWER(:key)"
			].join(' OR ')
			bonds.where(query_opts, {key: "%#{key}%"})
		end

		scope :filter_search, ->(params) do 
			return all if params.blank?
			filter_by_key(params[:key])
		end
	end

end