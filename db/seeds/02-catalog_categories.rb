module SeedCatalogCategory
	CATALOG_TITLE 		= ['Wordpress','HTML Template','CMS','Framework']
	CATALOG_WORDPRESS = [
												'Wp Company Profile','Wp E-Commerce',
												'Wp Goverment','Wp Portfolio Template',
												'Wp School','Wp Wedding','Wp University'
											]
	CATALOG_HTML 			= [
												'Company Profile','E-Commerce',
												'Goverment','Market Place',
												'Portfolio Template','School',
												'Social Media','Wedding','University'
											]
	CATALOG_CMS 			= ['Joomla','Magento']
	CATALOG_FRAMEWORK = ['Ruby on Rails','Codeigniter']
	
	def self.seed 
		CATALOG_TITLE.each do |cat|
			catalog = CatalogCategory.find_or_initialize_by(name: cat)
			catalog.save

			if cat == 'Wordpress'
				CATALOG_WORDPRESS.each do |child|
					child = CatalogCategory.find_or_initialize_by(name: child)
					child.ancestry = "#{catalog.id}"
					child.save
				end
			end

			if cat == 'HTML Template'
				CATALOG_HTML.each do |child|
					child = CatalogCategory.find_or_initialize_by(name: child)
					child.ancestry = "#{catalog.id}"
					child.save
				end
			end

			if cat == 'CMS'
				CATALOG_CMS.each do |child|
					child = CatalogCategory.find_or_initialize_by(name: child)
					child.ancestry = "#{catalog.id}"
					child.save
				end
			end

			if cat == 'Framework'
				CATALOG_FRAMEWORK.each do |child|
					child = CatalogCategory.find_or_initialize_by(name: child)
					child.ancestry = "#{catalog.id}"
					child.save
				end
			end
				
		end
	end
end