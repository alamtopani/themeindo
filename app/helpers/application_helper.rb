module ApplicationHelper

  def number_to_currency_br(number)
    number_to_currency(number, :unit => "$ ", :separator => ",", :delimiter => ".", precision: 0)
  end

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
        concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
          concat content_tag(:span, 'Close', class: 'sr-only')
        end)
        if flash[:errors].present?
          message.each do |word|
            concat "<div class='alert alert-danger alert-dismissible margin-bottom5'>#{word}</div>".html_safe
          end
        else
          concat message
        end
      end)
    end
    nil
  end

	def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def catalog_category_root
    @catalog_category_root ||= CatalogCategory.roots.alfa
  end

  def catalog_category_child(params)
    @catalog_category_child ||= CatalogCategory.find(params)
  end

  def catalog_type
    @catalog_type ||=
    [
      ["Free Item","template", data: { url: "#{search_url(params.merge(catalog_type: 'free_item'))}" }],
      ["Template","template", data: { url: "#{search_url(params.merge(catalog_type: 'template'))}" }],
      ["Plugin","plugin", data: { url: "#{search_url(params.merge(catalog_type: 'plugin'))}" }]
    ]
  end

  def layout_type
    @layout_type ||=
    [
      ["One Page","one_page", data: { url: "#{search_url(params.merge(layout_type: 'one_page'))}" }],
      ["Multi Purpose","multi_purpose", data: { url: "#{search_url(params.merge(layout_type: 'multi_purpose'))}" }]
    ]
  end

  def sort_by
  	@sort_by ||=
    [
      ["Newest Items", "latest", data: { url: "#{search_url(params.merge(sort_by: 'latest'))}" }],
      ["Oldest Items", "oldest", data: { url: "#{search_url(params.merge(sort_by: 'oldest'))}" }],
      ["Price: low to high", "low_to_high", data: { url: "#{search_url(params.merge(sort_by: 'low_to_high'))}" }],
      ["Price: high to low", "high_to_low", data: { url: "#{search_url(params.merge(sort_by: 'high_to_low'))}" }]
    ]
  end

  def published
  	@published ||=
    [
      ["In the last year","last_year", data: { url: "#{search_url(params.merge(published: 'last_year'))}" }],
      ["In the last month","last_month", data: { url: "#{search_url(params.merge(published: 'last_month'))}" }],
      ["In the last week","last_week", data: { url: "#{search_url(params.merge(published: 'last_week'))}" }],
      ["In the last day","last_day", data: { url: "#{search_url(params.merge(published: 'last_day'))}" }]
    ]
  end

  def rating
  	@rating ||=
    [
      ["1 Star and higher","one_star"],
      ["2 Star and higher","two_star"],
      ["3 Star and higher","three"],
      ["4 Star and higher","four_star"]
    ]
  end

  def software_version
    @software_version ||=
    [
      ['jQuery','jQuery'],
      ['MooTools','MooTools'],
      ['YUI','YUI'],
      ['EXT JS 3','EXT JS 3'],
      ['script.aculo.us','script.aculo.us'],
      ['Node.js','Node.js'],
      ['Other','Other']
    ]
  end

  def status
    @status ||= [
      ['Verified', true],
      ['Unverified', false]
    ]
  end

  def sex
    @sex ||= ['Male','Female']
  end

  def departement
    @departement ||= ['Technical Support','Billing and Payment','Sales/Marketing']
  end

  def priority
    @priority ||= ['High','Medium','Low']
  end

  def staff
    @staff ||= [
      ['Staff', true],
      ['Member', false]
    ]
  end

end
