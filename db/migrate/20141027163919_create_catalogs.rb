class CreateCatalogs < ActiveRecord::Migration
  def change
    create_table :catalogs do |t|
      t.integer :user_id
      t.string :title
      t.text :content
      t.decimal :price
      t.integer :columns
      t.string :software_version 
      t.string :compatible_browsers
      t.string :compatible_with
      t.string :document
      t.boolean :high_resolution
      t.string :layout
      t.string :files_included
      t.boolean :widget_ready
      t.string :tags
      t.integer :catalog_id

      t.timestamps
    end

    add_index :catalogs, :user_id
    add_index :catalogs, :title
  end
end
