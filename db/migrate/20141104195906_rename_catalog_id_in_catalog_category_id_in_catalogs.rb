class RenameCatalogIdInCatalogCategoryIdInCatalogs < ActiveRecord::Migration
  def change
  	rename_column :catalogs, :catalog_id, :catalog_category_id
  	add_index :catalogs, :catalog_category_id
  end
end
