class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    else
      can :manage, User, id: user.id
      can :manage, Catalog, user_id: user.id
      can :manage, Order, user_id: user.id
      can :manage, Ticket, user_id: user.id
    end
  end
end
