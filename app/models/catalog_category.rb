class CatalogCategory < ActiveRecord::Base
	include Tree
	include CacheIt
	extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :catalogs, foreign_key: 'catalog_category_type_id', dependent: :destroy
	has_many :catalogs, foreign_key: 'catalog_category_id', dependent: :destroy

	accepts_nested_attributes_for :catalogs, reject_if: :all_blank, allow_destroy: true

	scope :latest, -> {order("created_at DESC")}
	scope :oldest, -> {order("created_at ASC")}
	scope :alfa, -> {order("name ASC")}
end
