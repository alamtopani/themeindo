class User < ActiveRecord::Base
  extend FriendlyId
  include UserConfig
  include UserAuthenticate
  include UserSearch
  
  friendly_id :username, use: [:slugged, :finders]
  after_create :send_welcome_mail
  
  acts_as_votable
  is_impressionable

  def self.current
    Thread.current[:user]
  end
  
  def self.current=(user)
    Thread.current[:user] = user
  end

  def score
    self.get_upvotes.size
  end

  private

    def send_welcome_mail
      UserMailer.welcome_join(self).deliver
    end
end
