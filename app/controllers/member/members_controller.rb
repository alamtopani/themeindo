class Member::MembersController < Member::MemberApplicationController
	load_and_authorize_resource
	defaults resource_class: Member, collection_name: 'members', instance_name: 'member'
	include Member::UserFilter

	add_breadcrumb "Home", :root_path
	add_breadcrumb "Dashboard", :member_home_path
	add_breadcrumb "Profile", :profile_path

	def edit
		add_breadcrumb "Settings", edit_member_member_path(current_user)
	end

	def update
		update! do |format|
      if resource.errors.empty?
        format.html { redirect_to :back}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
    end
	end
end