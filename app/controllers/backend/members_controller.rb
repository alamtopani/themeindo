class Backend::MembersController < Backend::UsersController
	defaults resource_class: Member, collection_name: 'members', instance_name: 'member'
	include Backend::ActionMultiple

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def change_position
		resource.change_verified_status!
		if resource.staff?
			UserMailer.send_user_to_staff(resource).deliver
			redirect_to :back, notice: "#{resource.username} has change to staff successfully"
		else
			redirect_to :back, notice: "#{resource.username} has change to member successfully"
		end
	end

	def change_featured
		resource.change_featured_status!
		if resource.featured?
			UserMailer.send_user_to_featured(resource).deliver
			redirect_to :back, notice: "#{resource.username} has featured successfully"
		else
			redirect_to :back, notice: "#{resource.username} has unfeatured successfully"
		end
	end

	private
    def collection
      @members ||= end_of_association_chain.search_by(params)
    end
end