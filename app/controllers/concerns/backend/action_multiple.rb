module Backend::ActionMultiple
	extend ActiveSupport::Concern

	def action_multiple
		if params[:commit] == 'Send Subscribe'
			send_multiple_promo
		elsif params[:commit] == 'Deleted'
			destroy_multiple
		elsif params[:commit] == 'Verified'
			verified_multiple
		end	
	end

	def verified_multiple
		if params[:ids].present?
			@collection = collection.where("catalogs.id IN (?)", params[:ids])
			@collection.each do |catalog|
				catalog.change_verified_status!
				UserMailer.send_catalog_verified(catalog).deliver
			end
			redirect_to :back, notice: "You verified multiple items successfully change!"
		else
			redirect_to :back, alert: 'Please Choice Selected!'
		end
	end

	def destroy_multiple
		if params[:ids].present?
			@collection = collection.destroy(params[:ids])
			if @collection
				redirect_to :back, notice: 'Successfully Destroyed!'
			else
				redirect_to :back, alert: 'Not Successfully Destroyed!'
			end
		else
			redirect_to :back, alert: 'Please Choice Selected!'
		end
	end

	def send_multiple_promo
		if params[:ids].present?
			@collection = collection.where("catalogs.id IN (?)", params[:ids])
			subscribes = Subscribe.verified

			subscribes.each do |sub|
				catalog_ids = @collection.pluck(:id)

				catalogs = collection.where("catalogs.id IN (?)", catalog_ids)
				UserMailer.send_subscribe(sub, catalogs).deliver if catalogs.present? && subscribes.present?
			end
			redirect_to :back, notice: "You have send the email subscribes successfully!"
		else
			redirect_to :back, alert: 'Please Choice Selected!'
		end
	end
	
end