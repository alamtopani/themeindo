class AddBannerUserInUsers < ActiveRecord::Migration
	def self.up
    add_attachment :profiles, :banner_user
  end

  def self.down
    remove_attachment :profiles, :banner_user
  end
end
