module API
	module V1
		class CatalogsController < ResourcesController
			defaults resource_class: Catalog, collection_name: 'catalogs', instance_name: 'catalog'
			impressionist
			add_breadcrumb "Home", :root_path
			add_breadcrumb "Catalogs", :catalogs_path

			before_action :prepare_order, except: [:index, :upvote]

			def index
				@catalog_roots = collection.latest.verify.filter_search(params)
		    @catalogs = @catalog_roots.page(page).per(per_page)

	      render :json => @catalog_roots, status: 200
			end

			def show
				add_breadcrumb "#{resource.title}", catalog_path(resource)
				impressionist(resource, 'message', unique: [:session_hash])
		    if session[:order_id]
		      check_order = Order.find(session[:order_id])
		      @check_order = check_order.order_items.where(catalog_id: resource.id)
		    end
			end

			def comments
				add_breadcrumb "#{resource.title}", catalog_path(resource)
				add_breadcrumb "Comments", comments_catalog_path(resource)
				if params[:to].present?
					@checked_comment = Comment.find(params[:to])
					@checked_comment.update(checked: true)
					@checked_comment.save
				end

				@comment_root = resource.comments.new
				@comment = Comment.new
				@comments_root = resource.comments.verify.latest
				@comments = @comments_root.page(page).per(per_page)
			end

			def support
				add_breadcrumb "#{resource.title}", catalog_path(resource)
				add_breadcrumb "Support", support_catalog_path(resource)
			end

			def ticket
				add_breadcrumb "#{resource.title}", catalog_path(resource)
				add_breadcrumb "Support", support_catalog_path(resource)
				add_breadcrumb "Ticket", ticket_catalog_path(resource)
				@ticket = Ticket.new
			end

			def upvote
			  @link = Catalog.find(params[:id])
			  @success = @link.upvote_by current_user
			  if @success
			  	redirect_to :back, alert: "Your already like this item!"
			  else
			  	redirect_to :back, error: "Your not success like this item!"
			  end
			end

			private
				def prepare_order
					@order_item = current_order.order_items.new
				end
			
		end
	end
end