require 'paypal-sdk-merchant'
 
class PaypalInterface
 
	attr_reader :api, :express_checkout_response

  PAYPAL_RETURN_URL = Rails.application.routes.url_helpers.paid_orders_url(host: HOST_WO_HTTP)
  PAYPAL_CANCEL_URL = Rails.application.routes.url_helpers.revoked_orders_url(host: HOST_WO_HTTP)
  PAYPAL_NOTIFY_URL = Rails.application.routes.url_helpers.ipn_orders_url(host: HOST_WO_HTTP)

  def initialize(order)
    @api = PayPal::SDK::Merchant::API.new
    @order = order
  end

  def express_checkout
    order_items = []
    @order.order_items.each do |item|
      each_price = (item.catalog.price * 100).round
      hash = {}
      hash.merge!( Name: item.catalog.title, Quantity: item.quantity, Amount: {
                                                                                currencyID: "USD", 
                                                                                value: item.catalog.price 
                                                                              },
                                                                              ItemCategory: "Physical"
                  )
     
      order_items << hash
    end
    
    @set_express_checkout = @api.build_set_express_checkout({
      SetExpressCheckoutRequestDetails: {
        ReturnURL: PAYPAL_RETURN_URL,
        CancelURL: PAYPAL_CANCEL_URL,
        BrandName: 'Themeindo',
        PaymentDetails: [{
          NotifyURL: PAYPAL_NOTIFY_URL,
          OrderTotal: {  
                        currencyID: "USD", 
                        value: @order.total
                      },
          ItemTotal: {
                        currencyID: "USD",
                        value: @order.total
                      },
          PaymentDetailsItem: order_items,
          PaymentAction: "Sale"
        }]
      }
    })

    # Make API call & get response
    @express_checkout_response = @api.set_express_checkout(@set_express_checkout)

    # Access Response
    if @express_checkout_response.success?
      @order.set_payment_token(@express_checkout_response.Token)
    else
      @express_checkout_response.Errors
    end
  end

  def do_express_checkout
    @do_express_checkout_payment = @api.build_do_express_checkout_payment({
      DoExpressCheckoutPaymentRequestDetails: {
        PaymentAction: "Sale",
        Token: @order.payment_token,
        PayerID: @order.payerID,
        PaymentDetails: [{
          OrderTotal: {
            currencyID: "USD",
            value: @order.total
          },
          NotifyURL: PAYPAL_NOTIFY_URL
        }]
      }
    })

    # Make API call & get response
    @do_express_checkout_payment_response = @api.do_express_checkout_payment(@do_express_checkout_payment)

    # Access Response
    if @do_express_checkout_payment_response.success?
      details = @do_express_checkout_payment_response.DoExpressCheckoutPaymentResponseDetails

      # transaction_id = details.PaymentInfo[0].TransactionID
      # @order.set_transaction(transaction_id)
    else
      errors = @do_express_checkout_payment_response.Errors
      @order.save_payment_errors errors
    end
  end

  def get_express_checkout
    # Build request object
    @get_express_checkout_details = @api.build_get_express_checkout_details({
      Token: @order.payment_token 
    })

    # Make API call & get response
    @get_express_checkout_details_response = @api.get_express_checkout_details(@get_express_checkout_details)

    # Access Response
    if @get_express_checkout_details_response.success?
      get_details = @get_express_checkout_details_response.GetExpressCheckoutDetailsResponseDetails
      transaction_id = get_details.PaymentRequestInfo[0].TransactionId
      @order.set_transaction(transaction_id, get_details.PayerInfo.Address.Name, get_details.PayerInfo.Payer, get_details.PayerInfo.Address.CityName+ ', ' +get_details.PayerInfo.Address.CountryName)
    else
      @get_express_checkout_details_response.Errors
    end
  end
 
end
