class LandingPage < ActiveRecord::Base
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

	scope :verified, -> {where(status: true)}
	
	scope :latest, -> {order("created_at DESC")}
	scope :oldest, -> {order("created_at ASC")}
end
