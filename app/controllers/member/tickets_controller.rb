class Member::TicketsController < Member::MemberApplicationController
	load_and_authorize_resource
	defaults resource_class: Ticket, collection_name: 'tickets', instance_name: 'ticket'
	include Backend::ActionMultiple

	add_breadcrumb "Home", :root_path
	add_breadcrumb "Dashboard", :member_home_path
	add_breadcrumb "Tickets", :member_tickets_path

	def home
	end
	
	def index		
		@tickets = collection.latest.where(user_id: current_user.id).filter_search(params)
		@collection = @tickets.page(page).per(per_page)
	end

	def new
		add_breadcrumb "New"
		build_resource
	end

	def show
		add_breadcrumb "Show"
		@messages = resource.messages.oldest
		@message = Message.new
	end

	def create
		build_resource
    resource.user_id = current_user.id
    resource.save
    create! do |format|
      if resource.errors.empty?
      	UserMailer.send_ticket(resource).deliver
        format.html { redirect_to member_tickets_path}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
    end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end