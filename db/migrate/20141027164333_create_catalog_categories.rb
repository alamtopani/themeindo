class CreateCatalogCategories < ActiveRecord::Migration
  def change
    create_table :catalog_categories do |t|
      t.string :name

      t.timestamps
    end

    add_index :catalog_categories, :name
  end
end
