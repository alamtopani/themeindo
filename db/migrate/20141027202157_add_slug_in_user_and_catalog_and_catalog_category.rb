class AddSlugInUserAndCatalogAndCatalogCategory < ActiveRecord::Migration
  def change
  	add_column :users, :slug, :string
  	add_column :catalogs, :slug, :string
  	add_column :catalog_categories, :slug, :string

  	add_index :users, :slug
  	add_index :catalogs, :slug
  	add_index :catalog_categories, :slug
  end
end
