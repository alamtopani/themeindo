class Backend::OrdersController < Backend::BackendApplicationController
	defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'
	include Backend::ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def change_order_status
		@order = Order.find(params[:id])
		@order.update(order_status_id: params[:order][:order_status_id])
		if @order.save
			redirect_to :back, alert: 'Order status has changed'
		else
			redirect_to :back, error: 'Order status not changed'
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end