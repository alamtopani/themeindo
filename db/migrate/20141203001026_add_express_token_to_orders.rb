class AddExpressTokenToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :payment_token, :string 
  	add_column :orders, :payerID, :string
  end
end
