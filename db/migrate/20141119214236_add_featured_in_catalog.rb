class AddFeaturedInCatalog < ActiveRecord::Migration
  def change
  	add_column :catalogs, :featured, :boolean
  end
end
