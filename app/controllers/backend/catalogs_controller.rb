class Backend::CatalogsController < Backend::BackendApplicationController
	defaults resource_class: Catalog, collection_name: 'catalogs', instance_name: 'catalog'
	include Backend::ActionMultiple
	
	def index
		@catalogs =  collection.last_updated
		@collection = @catalogs.page(page).per(per_page)

		respond_to do |format|
      format.html
      format.xls
    end
	end

	def create
		build_resource
    resource.catalog_category_id = params[:catalog_category_id]
    resource.user_id = current_user.id
    resource.save
    create! do |format|
      if resource.errors.empty?
        format.html { redirect_to backend_catalogs_path}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
    end
	end

	def update
		resource.catalog_category_id = params[:catalog_category_id]
		resource.save
		update! do |format|
      if resource.errors.empty?
        format.html { redirect_to :back}
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back}
      end
    end
	end

	def change_verified
		resource.change_verified_status!
		if resource.published?
			UserMailer.send_catalog_verified(resource).deliver
			redirect_to :back, notice: "#{resource.title} has verified successfully"
		else
			redirect_to :back, notice: "#{resource.title} has unverified successfully"
		end
	end

	def change_featured
		resource.change_featured_status!
		if resource.featured?
			UserMailer.send_catalog_featured(resource).deliver
			redirect_to :back, notice: "#{resource.title} has featured successfully"
		else
			redirect_to :back, notice: "#{resource.title} has unfeatured successfully"
		end
	end

	def document_download
	  @catalog = Catalog.find_by(code: params[:code])
    url = @catalog.file_catalog.url
    file_path = url.split("?").first
    if !file_path.nil?
			send_file "#{Rails.root}/public#{file_path}", :x_sendfile => true 
		else 
      redirect_to :back
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end