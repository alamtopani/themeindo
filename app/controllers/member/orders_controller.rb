class Member::OrdersController < Member::MemberApplicationController
	load_and_authorize_resource
	defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'

	add_breadcrumb "Home", :root_path
	add_breadcrumb "Dashboard", :member_home_path
	add_breadcrumb "Invoice Orders", :member_orders_path

	def index
		@orders = collection.latest.where(user_id: current_user.id)
		@collection = @orders.page(page).per(per_page)
	end

	def show
		add_breadcrumb "Show"

		@order = collection.find(params[:id])
	end

	def destroy
		@resource = collection.find(params[:id])
		if @resource.destroy
			if session[:order_id] == @resource.id
				session[:order_id] = nil
			end
			redirect_to member_orders_path, alert: "You already cancel invoice order #{@resource.code}"
		else
			redirect_to member_orders_path, error: "Failed, you can't cancel"
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end