class Subscribe < ActiveRecord::Base
	belongs_to :user

	before_save :prepare_status

	scope :verified, -> {where(status: true)}
	scope :latest, -> {order(created_at: :desc)}
	scope :oldest, -> {order(created_at: :asc)}

	validates_uniqueness_of :email

	def status?
    return '<span class="label label-success">Already Verified</span>'.html_safe if self.status == true
    return '<span class="label label-warning">Not Yet Verified</span>'.html_safe if self.status == false
  end

  private
  	include SubscribeSearch

  	def prepare_status
  		self.status = true
  	end
end
