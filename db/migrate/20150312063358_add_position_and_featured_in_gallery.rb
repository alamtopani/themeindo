class AddPositionAndFeaturedInGallery < ActiveRecord::Migration
  def change
  	add_column :galleries, :position, :integer
  	add_column :galleries, :featured, :boolean
  end
end
