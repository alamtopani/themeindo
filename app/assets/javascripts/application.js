// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require jquery.isotope
//= require customize
//= require cocoon
//= require jquery.raty
//= require jquery.easypiechart
//= require ratyrate
//= require ckeditor/init
//= require ckeditor/config
//= require bootstrap-magnify
//= require jquery-lazyload
//= require versions2/jquery-scrolltofixed-min
//= require versions2/isotope.min
//= require versions2/theme
//= require versions2/pinterest-grid-plugin
//= require versions2/lightbox

$(document).ready(function(){
	$("img").show().lazyload({
     event: "lazyload",
     effect: "fadeIn",
     effectspeed: 500
  })
  .trigger("lazyload");
  
  $('form[data-remote]').bind('ajax:before', function(){
    for (instance in CKEDITOR.instances){
      CKEDITOR.instances[instance].updateElement();
    }
  });

});
