class Backend::UsersController < Backend::BackendApplicationController
	defaults resource_class: User, collection_name: 'users', instance_name: 'user'
	include Backend::UserFilter

	def index
		@collection = collection.page(page).per(per_page)
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end
end