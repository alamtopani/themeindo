# require 'paperclip/media_type_spoof_detector'
# module Paperclip
#   class MediaTypeSpoofDetector
#     def spoofed?
#       false
#     end
#   end
# end

Paperclip.options[:content_type_mappings] = {
  :png => "application/octet-stream",
  :jpg => "application/octet-stream",
  :jpeg => "application/octet-stream"
}

Paperclip::Attachment.default_options[:use_timestamp] = false