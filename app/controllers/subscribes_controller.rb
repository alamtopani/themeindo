class SubscribesController < ResourcesController
	defaults resource_class: Subscribe, collection_name: 'subscribes', instance_name: 'subscribe'

	def create
	  create! do |format|
	  	if resource.errors.empty?
        flash[:alert] = 'Your Successfully Registered Subscribe Themeindo'
        format.html { redirect_to :back }
      else 
        flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back }
      end
	  end
	end

	private
    def collection
      @subscribes ||= end_of_association_chain
    end
end