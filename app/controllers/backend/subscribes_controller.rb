class Backend::SubscribesController < Backend::BackendApplicationController
	defaults resource_class: Subscribe, collection_name: 'subscribes', instance_name: 'subscribe'
	include Backend::ActionMultiple

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	protected
    def collection
      @collection ||= end_of_association_chain.filter_search(params)
   	end
end