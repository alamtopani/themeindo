class Backend::BackendApplicationController < Backend::ResourcesController
	layout 'backend'
	before_filter :authenticate_admin!
	protect_from_forgery with: :exception

	def authenticate_admin!
  	unless current_user.present? && (current_user.admin?)
      redirect_to root_path 
    end 
  end
end