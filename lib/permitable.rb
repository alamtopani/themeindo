module Permitable
	USER = [
      :id,
      :type,
      :username, 
      :email, 
      :password, 
      :password_confirmation, 
      :provider, 
      :uid, 
      :oauth_token, 
      :oauth_expires_at,
      :slug,
      :staff,
      :featured
    ]

  ADDRESS = {
      address_attributes: [
        :id,
        :address,
        :city,
        :province,
        :country,
        :postcode,
        :addressable_type,
        :addressable_id,
        :addressable,
        :facebook_url,
        :twitter_url
      ]
  }

  PROFILE = {
      profile_attributes: [
        :id,
        :user_id,
        :full_name,
        :birthday,
        :gender,
        :phone,
        :avatar,
        :banner_user,
        :about_me
      ].push(ADDRESS.dup)
  }

  CATALOG_CATEGORY = [
      :id,
  		:name,
      :slug,
      :ancestry,
      :parent_id
  ]

  CATALOG = [
      :id,
      :user_id,
      :title,
      :content,
      :price,
      :columns,
      :software_version,
      :compatible_browsers,
      :compatible_with,
      :document,
      :high_resolution,
      :layout,
      :files_included,
      :widget_ready,
      :tags,
      :catalog_category_type_id,
      :catalog_category_id,
      :slug,
      :status,
      :catalog_type,
      :layout_type,
      :demo_url,
      :featured,
      :file_catalog,
      :code,
      :thumbnail
  ]

  GALLERY = {
      galleries_attributes: [
        :id,
        :title,
        :description,
        :file,
        :galleriable_type,
        :galleriable_id,
        :galleriable,
        :position,
        :featured,
        :_destroy
      ]
  }

  COMMENT = [
      :id,
      :user_id,
      :commentable_type,
      :commentable_id,
      :commentable,
      :comment,
      :status,
      :ancestry,
      :parent_id,
      :checked
  ]

  WEB_SETTING = [
      :id,
      :title,
      :description,
      :keywords,
      :header_tags,
      :footer_tags,
      :contact,
      :email,
      :favicon,
      :logo,
      :facebook,
      :twitter
  ]

  TICKET = [
      :id,
      :user_id,
      :name,
      :email,
      :title,
      :departement,
      :problem,
      :priority,
      :message,
      :status,
      :slug,
      :code
  ]

  FAVORITE = [
      :user_id,
      :catalog_id
  ]

  ORDER_ITEM = [
      :id,
      :catalog_id,
      :order_id,
      :price,
      :quantity,
      :total_price,
      :user_id,
      :seller_id,
      :order_item_status
  ]

  ORDER = [
      :id,
      :code,
      :user_id,
      :order_status_id,
      :token,
      :total,
      :payment_token,
      :payerID,
      :ip,
      :purchased_at,
      :transaction_id,
      :slug,
      :payer_name,
      :payer_email,
      :payer_address
  ]

  MESSAGE = [
      :user_id,
      :to,
      :ticket_id,
      :subject,
      :message,
      :status,
      :slug
  ]

  LANDING_PAGE = [
      :title,
      :slug,
      :description,
      :status,
      :category
  ]

  SUBSCRIBE = [
      :email,
      :status
  ]

  TESTIMONIAL = [
      :user_id,
      :description,
      :status
  ]

  def self.controller(name)
    self.send name.gsub(/\W/,'_').singularize.downcase
  end

  def self.backend_user
    USER.dup.push(PROFILE.dup)
  end

  def self.backend_admin
    backend_user
  end

  def self.backend_member
    backend_user
  end

  def self.backend_catalog_category
  	CATALOG_CATEGORY.dup
  end

  def self.backend_catalog
    CATALOG.dup.push(GALLERY.dup)
  end

  def self.backend_comment
    COMMENT.dup
  end

  def self.backend_ticket
    TICKET.dup
  end

  def self.backend_favorite
    FAVORITE.dup
  end

  def self.backend_order
    ORDER.dup
  end

  def self.backend_message
    MESSAGE.dup
  end

  def self.backend_web_setting
    WEB_SETTING.dup.push(GALLERY.dup)
  end

  def self.backend_landing_page
    LANDING_PAGE.dup
  end

  def self.backend_subscribe
    SUBSCRIBE.dup
  end

  def self.backend_testimonial
    TESTIMONIAL.dup
  end

  def self.comment
    COMMENT.dup
  end

  def self.ticket
    backend_ticket
  end

  def self.testimonial
    backend_testimonial
  end

  def self.member_member
    backend_user
  end

  def self.member_catalog
    backend_catalog
  end

  def self.member_ticket
    backend_ticket
  end

  def self.member_favorite
    backend_favorite
  end

  def self.member_order
    backend_order
  end

  def self.member_message
    backend_message
  end

  def self.order
    backend_order
  end

  def self.order_item
    ORDER_ITEM.dup
  end

  def self.subscribe
    SUBSCRIBE.dup
  end

end