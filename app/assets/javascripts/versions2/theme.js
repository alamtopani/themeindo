$(window).load(function(){
// Top Menu Target
 $('#top-navigation').scrollToFixed();
 // $('#top-navigation li a[href*=#]:not([href=#])').click(function() {
 //    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
 //      var target = $(this.hash);
 //      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
 //      if (target.length) {
 //        $('html,body').animate({
 //          scrollTop: target.offset().top
 //        }, 1000);
 //        return false;
 //      }
 //    }
 //  });

  // ScrollTopUp
  // var offset=220;
  // var duration=500;
  // jQuery(window).scroll(function(){
  //   if(jQuery(this).scrollTop()>offset){
  //     jQuery('.toup').css({opacity:"1",display:"block",});
  //   }else{
  //     jQuery('.toup').css('opacity','0');
  //   }
  // });
  
  // jQuery('.toup').click(function(event){
  //   event.preventDefault();
  //   jQuery('html, body').animate({scrollTop:0},duration);
  //   return false;
  // });

  $(window).scroll(function(){
    if ($(this).scrollTop() > 120) {
      $('.scrollup').fadeIn(400);
    } else {
      $('.scrollup').fadeOut(400);
    }
  }); 

  $('.scrollup').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 800);
    return false;
  });

  // Responsive Slider
  $(window).resize(function(){
    $('.image-responsive').each(function(){
      if( $(this).height == 0 ){
        var imgWidth = $(this).width() / 1.7;
        $(this).css('height', imgWidth);
      }else{
        var imgWidth = $(this).width() / 1.7;
        $(this).css('height', imgWidth);
      }
    });

    $('.home-carousel .item').each(function(){
      var topSlider = $('.home-carousel').outerWidth() / 3.2;
      $(this).css('height', topSlider);
    });
  }).resize();

  // Dropdown Menu Custom
  $(".dropdown").hover(            
    function() {
      $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
      $(this).toggleClass('open');        
    },
    function() {
      $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
      $(this).toggleClass('open');       
    }
  );

  // Parallax
  var $bgobj = $(".ha-bg-parallax"); // assigning the object
  $(window).on("scroll", function () {
    var yPos = -($(window).scrollTop() / $bgobj.data('speed'));
    var coords = '100% ' + yPos + 'px';
    $bgobj.css({ backgroundPosition: coords });
  });

  // Column Gallery Custom
  var my_posts = $("[rel=tooltip]");
  var size = $(window).width();
  for(i=0;i<my_posts.length;i++){
    the_post = $(my_posts[i]);

    if(the_post.hasClass('invert') && size >=767 ){
      the_post.tooltip({ placement: 'left'});
      the_post.css("cursor","pointer");
    }else{
      the_post.tooltip({ placement: 'rigth'});
      the_post.css("cursor","pointer");
    }
  }   

  $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).Lightbox();
  });


  // Isotope Custom
  var $container = $('.isotope-container');
  $container.isotope({
    filter: '*',
    animationOptions: {
      duration: 750,
      easing: 'linear',
      queue: false
    }
  });
  
  $('.filters button').click(function(){
    var selector = $(this).attr('data-filter');
    $container.isotope({
      filter: selector,
      animationOptions: {
        duration: 750,
        easing: 'linear',
        queue: false
      }
    });
    return false;
  });

});
