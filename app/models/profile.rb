class Profile < ActiveRecord::Base
	after_initialize :after_initialized

	belongs_to :user, foreign_key: 'user_id'

	has_one :address, as: :addressable, dependent: :destroy
	accepts_nested_attributes_for :address, reject_if: :all_blank

	has_attached_file :avatar, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :avatar, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  has_attached_file :banner_user, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :banner_user, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

	protected
		def after_initialized
			self.address = Address.new if self.address.blank?
		end
end
