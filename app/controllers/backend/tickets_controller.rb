class Backend::TicketsController < Backend::BackendApplicationController
	defaults resource_class: Ticket, collection_name: 'tickets', instance_name: 'ticket'
	include Backend::ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def show
		@message = Message.new
		@messages = resource.messages.oldest
	end

	protected
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end