class Favorite < ActiveRecord::Base
	belongs_to :user, foreign_key: 'user_id'
	belongs_to :catalog, foreign_key: 'catalog_id'

	scope :latest, -> {order(created_at: :desc)}
	scope :oldest, -> {order(created_at: :asc)}
end
